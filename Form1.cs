﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;

namespace wfaTimerr
{
    public partial class Form1 : Form
    {
        System.Timers.Timer timer;
        int hours = 0, minutes = 0, seconds = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            timer = new System.Timers.Timer();
            timer.Interval = 1000; // 1 sec
            timer.Elapsed += OnTimeEvents;
        }

        private void OnTimeEvents(object sender, ElapsedEventArgs e)
        {
            Invoke(new Action(() =>
            {
                seconds += 1;
                if (seconds == 60)
                {
                    seconds = 0;
                    minutes += 1;
                }
                if (minutes == 60)
                {
                    minutes = 0;
                    hours += 1;
                }
                txtTime.Text = string.Format("{0}:{1}:{2}", hours.ToString().PadLeft(2, '0'),
                                                            minutes.ToString().PadLeft(2, '0'),
                                                            seconds.ToString().PadLeft(0, '0'));
            }));
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            txtTime.Text = string.Format("{0}:{1}:{2}", hours.ToString().PadLeft(2, '0'),
                                                            minutes.ToString().PadLeft(2, '0'),
                                                            seconds.ToString().PadLeft(0, '0'));

            timer.Start();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            hours = 0;
            minutes = 0;
            seconds = 0;

            txtTime.Text = string.Format("{0}:{1}:{2}", hours.ToString().PadLeft(2, '0'),
                                                            minutes.ToString().PadLeft(2, '0'),
                                                            seconds.ToString().PadLeft(0, '0'));

            timer.Stop();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer.Stop();
            Application.DoEvents();
        }
    }
}
